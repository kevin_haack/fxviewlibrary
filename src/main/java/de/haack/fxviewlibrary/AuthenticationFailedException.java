package de.haack.fxviewlibrary;

/**
 *
 * @author Kevin Haack
 */
public class AuthenticationFailedException extends Exception {

    public AuthenticationFailedException(String message) {
        super(message);
    }
    
    public AuthenticationFailedException() {
        super( "Authentification failed");
    }
}
