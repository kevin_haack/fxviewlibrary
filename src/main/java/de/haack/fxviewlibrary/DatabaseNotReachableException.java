package de.haack.fxviewlibrary;

/**
 *
 * @author Kevin Haack
 */
public class DatabaseNotReachableException extends Exception {
    public DatabaseNotReachableException(Exception ex) {
        super(ex);
    }
}
