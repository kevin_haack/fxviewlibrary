package de.haack.fxviewlibrary.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 *
 * @author Kevin Haack, Robin Marchel
 */
public abstract class AbstractController implements Initializable {

    /**
     * The count of shown progresses.
     */
    private int progressIndicatorCounter = 0;
    /**
     * The current viewConfig object.
     */
    private ViewConfig viewConfig;
    /**
     * The stage.
     */
    private Stage stage;
    /**
     * The progressIndicator
     */
    private Node progressIndicator;
    /**
     * The content of the scene, without progressIndicator and the wrapping
     * stackPane.
     */
    private Node content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Methode to be called, after the fxml initialization.
     */
    public abstract void onPostInitialization();
    
    /**
     * Methode to be called, after loading.
     */
    public abstract void onPostLoad();
    
    /**
     * Methode to be called, on reset the view.
     */
    public abstract void onReset();

    /**
     * Set the progressIndicator.
     *
     * @param progressIndicator progressIndicator.
     */
    public void setProgressIndicator(Node progressIndicator) {
        this.progressIndicator = progressIndicator;
    }

    /**
     * Set the content.
     *
     * @param content
     */
    public void setContent(Node content) {
        this.content = content;
    }

    /**
     * Return the stage.
     *
     * @return stage.
     */
    protected Stage getStage() {
        return stage;
    }

    /**
     * Set the stage.
     *
     * @param stage stage.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Show the ProgressIndicator. Increase the 'progressIndicatorCounter', when
     * 'show' is true and decrease the 'progressIndicatorCounter' if 'show' is
     * false. When the 'progressIndicatorCounter' reach zero, the
     * 'progressIndicator' will hide.
     *
     * @param show show.
     */
    protected synchronized void showProgressIndicator(boolean show) {
        if (show) {
            this.progressIndicatorCounter++;
        } else {
            if (this.progressIndicatorCounter > 0) {
                this.progressIndicatorCounter--;
            } else {
                this.progressIndicatorCounter = 0;
            }
        }

        if (this.progressIndicatorCounter != 0) {
            // show
            this.progressIndicator.toFront();
            this.progressIndicator.setVisible(true);
            this.content.setDisable(true);
        } else {
            // hide
            this.progressIndicator.toBack();
            this.progressIndicator.setVisible(false);
            this.content.setDisable(false);
        }
    }

    /**
     * Return the viewConfig.
     *
     * @return viewConfig.
     */
    protected ViewConfig getViewConfig() {
        return viewConfig;
    }

    /**
     * Set the viewConfig.
     *
     * @param viewConfig viewConfig.
     */
    public void setViewConfig(ViewConfig viewConfig) {
        this.viewConfig = viewConfig;
    }
}
