package de.haack.fxviewlibrary.view.controls;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

    private final ComboBox comboBox;
    private final ObservableList<T> unfilteredList;
    private boolean moveCaretToPos = false;
    private int caretPos;

    public AutoCompleteComboBoxListener(final ComboBox comboBox) {
        this.comboBox = comboBox;
        this.unfilteredList = comboBox.getItems();
    }

    @Override
    public void handle(KeyEvent event) {

        // move selection
        if (event.getCode() == KeyCode.UP) {
            // UP
            caretPos = -1;
            moveCaret(comboBox.getEditor().getText().length());
            return;
        } else if (event.getCode() == KeyCode.DOWN) {
            // DOWN
            if (!comboBox.isShowing()) {
                comboBox.show();
            }
            caretPos = -1;
            moveCaret(comboBox.getEditor().getText().length());
            return;
        } else if (event.getCode() == KeyCode.BACK_SPACE) {
            // BACKSPACE
            moveCaretToPos = true;
            caretPos = comboBox.getEditor().getCaretPosition();
        } else if (event.getCode() == KeyCode.DELETE) {
            // DELETE
            moveCaretToPos = true;
            caretPos = comboBox.getEditor().getCaretPosition();
        } else if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                || event.isControlDown() || event.getCode() == KeyCode.HOME
                || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB) {
            // cancel selection
            return;
        }

        /*
         * filter items.
         */ 
        ObservableList filteredList = FXCollections.observableArrayList();
        for (int i = 0; i < unfilteredList.size(); i++) {
            if (null != unfilteredList.get(i).toString()
                    && unfilteredList.get(i).toString().toLowerCase().contains(AutoCompleteComboBoxListener.this.comboBox
                            .getEditor().getText().toLowerCase())) {
                filteredList.add(unfilteredList.get(i));
            }
        }
        String t = comboBox.getEditor().getText();

        comboBox.setItems(filteredList);
        
        if (!moveCaretToPos) {
            caretPos = -1;
        }
        moveCaret(t.length());
        
        if (!filteredList.isEmpty()) {
            comboBox.show();
        }
    }
    
    private void moveCaret(int textLength) {
        if (caretPos == -1) {
            comboBox.getEditor().positionCaret(textLength);
        } else {
            comboBox.getEditor().positionCaret(caretPos);
        }
        moveCaretToPos = false;
    }

}
