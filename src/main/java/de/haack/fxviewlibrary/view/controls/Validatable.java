package de.haack.fxviewlibrary.view.controls;

import javafx.beans.property.BooleanProperty;

/**
 * Indicates, that the implementing class is validatable.
 *
 * @author Kevin
 */
public interface Validatable {

    /**
     * Return the valid property.
     * @return 
     */
    public abstract BooleanProperty validProperty();

    /**
     * perform a manual validation.
     */
    public abstract void validate();
}
