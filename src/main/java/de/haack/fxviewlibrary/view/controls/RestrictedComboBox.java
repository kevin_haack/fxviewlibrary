package de.haack.fxviewlibrary.view.controls;

import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;

/**
 *
 * @author Kevin Haack
 */
public class RestrictedComboBox<T> extends ComboBox<T> implements Validatable {

    private final StringProperty requiredTextProperty = new SimpleStringProperty();
    private final ListProperty<String> validationErrors = new SimpleListProperty(FXCollections.observableArrayList());

    private final BooleanProperty validProperty = new SimpleBooleanProperty(this, "valid");
    private final BooleanProperty requiredProperty = new SimpleBooleanProperty(this, "required");

    /**
     * Tooltip for validation errors.
     */
    private final Tooltip tooltip = new Tooltip();

    public RestrictedComboBox() {
        /*
         * valid on empty validationErrors or temporarily on focus
         */
        validProperty().bind(validationErrors.emptyProperty()
                .or(this.focusedProperty())
                .or(this.disabledProperty()));

        /*
         * validation errors
         */
        validationErrors.addListener((ListChangeListener.Change<? extends String> listener) -> {
            if (!listener.getList().isEmpty()) {
                /*
                 * @TODO Über CSS regeln
                 */
                final StringBuilder errorBuilder = new StringBuilder();
                listener.getList().forEach(error -> errorBuilder.append(error).append("\n"));

                tooltip.setText(errorBuilder.toString());

                showValidationErrors();
                this.setTooltip(this.tooltip);
            } else {
                setTooltip(null);
                hideValidationErrors();
            }
        });

        /*
         * validate
         */
        focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            // only validate when focuslost
            if (oldValue && !newValue) {
                validate();
            } else {
                hideValidationErrors();
            }
        });
    }

    /**
     * Reset the comboBox. Clear the selection and validate it.
     */
    public void reset() {
        getSelectionModel().select(null);
        validate();
        hideValidationErrors();
    }

    /**
     * Scroll in the popup window to the top.
     */
    public void scrollToTop() {
        if(null != this.getSkin()
                && null != ((ComboBoxListViewSkin) this.getSkin()).getListView()) {
            ((ComboBoxListViewSkin) this.getSkin()).getListView().scrollTo(0);
        }
    }

    private void showValidationErrors() {
        this.getStyleClass().add("combo-box-error");
    }

    private void hideValidationErrors() {
        this.getStyleClass().remove("combo-box-error");
    }

    @Override
    public void validate() {
        validationErrors.clear();

        if (requiredProperty().get()) {
            if (null != getSelectionModel().selectedItemProperty()) {
                if (!getSelectionModel().selectedItemProperty().isNotNull().get()) {
                    getValidationErrors().add(requiredTextProperty.get());
                }
            }
        }
    }

    /*
     * required
     */
    public final void setRequired(boolean value) {
        requiredProperty().set(value);
    }

    public final boolean getRequired() {
        return this.requiredProperty.getValue();
    }

    public final BooleanProperty requiredProperty() {
        return this.requiredProperty;
    }

    /*
     * valid
     */
    public final void setValid(boolean value) {
        validProperty().set(value);
    }

    public final boolean getValid() {
        return this.validProperty.getValue();
    }

    @Override
    public final BooleanProperty validProperty() {
        return this.validProperty;
    }

    /*
     * requiredText
     */
    public final void setRequiredText(String value) {
        requiredTextProperty().set(value);
    }

    public final String getRequiredText() {
        return this.requiredTextProperty.getValue();
    }

    public final StringProperty requiredTextProperty() {
        return this.requiredTextProperty;
    }

    /*
     * validation errors
     */
    public final void setValidationErrors(ObservableList<String> value) {
        validationErrorsProperty().set(value);
    }

    public final ObservableList<String> getValidationErrors() {
        return this.validationErrors.getValue();
    }

    public final ListProperty<String> validationErrorsProperty() {
        return this.validationErrors;
    }
}
