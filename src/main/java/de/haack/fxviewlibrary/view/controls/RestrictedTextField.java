package de.haack.fxviewlibrary.view.controls;

import de.haack.fxviewlibrary.view.validation.AbstractRestriction;
import de.haack.fxviewlibrary.view.validation.AbstractValidator;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;

/**
 * @author Kevin Haack
 */
public class RestrictedTextField extends TextField implements Validatable {

    /**
     * textfield valid.
     */
    private final BooleanProperty validProperty = new SimpleBooleanProperty(this, "valid");
    /**
     * The current validation errors.
     */
    private final ListProperty<String> validationErrors = new SimpleListProperty(this, "validationErrors");
    /**
     * validators.
     */
    private final ObjectProperty<ObservableList<AbstractValidator>> validators = new SimpleObjectProperty<>(this, "validators");
    /**
     * restrictors.
     */
    private final ObjectProperty<ObservableList<AbstractRestriction>> restrictions = new SimpleObjectProperty<>(this, "restriction");
    /**
     * ignor the restrictions, to prevent infinity loops.
     */
    private boolean ignoreRestrictions;
    /**
     * Tooltip for validation errors.
     */
    private final Tooltip tooltip = new Tooltip();

    public RestrictedTextField() {
        // init collections.
        setValidators(FXCollections.observableArrayList());
        setRestrictions(FXCollections.observableArrayList());
        setValidationErrors(FXCollections.observableArrayList());

        /*
         * valid on empty validationErrors or temporarily on focus
         */
        validProperty().bind(validationErrorsProperty().emptyProperty()
                .or(this.focusedProperty())
                .or(this.disabledProperty()));

        /*
         * validation errors
         */
        validationErrorsProperty().addListener((ListChangeListener.Change<? extends String> listener) -> {
            if (!listener.getList().isEmpty()) {
                /*
                 * @TODO Über CSS regeln
                 */
                final StringBuilder errorBuilder = new StringBuilder();
                listener.getList().forEach(error -> errorBuilder.append(error).append("\n"));

                tooltip.setText(errorBuilder.toString());

                showValidationErrors();
                this.setTooltip(this.tooltip);
            } else {
                setTooltip(null);
                hideValidationErrors();
            }
        });

        /*
         * validate on validators changed
         */
        validators.addListener((ObservableValue<? extends ObservableList<AbstractValidator>> observable, ObservableList<AbstractValidator> oldValue, ObservableList<AbstractValidator> newValue) -> {
            validate();
            hideValidationErrors();
        });

        /*
         * validators
         */
        focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            // only validate when focuslost
            if (oldValue && !newValue) {
                validate();
            } else {
                hideValidationErrors();
            }
        });

        /*
         * restritions 
         */
        textProperty().addListener((ObservableValue<? extends String> observableValue, String oldString, String newString) -> {
            if (ignoreRestrictions || newString == null) {
                return;
            }

            checkRestriction(oldString, newString);
        });
    }

    private void showValidationErrors() {
        Border border = new Border(new BorderStroke(javafx.scene.paint.Paint.valueOf("Red"),
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                BorderWidths.DEFAULT));
        this.setBorder(border);
    }

    /**
     * Reset the textField. Clear the text and validate it.
     */
    public void reset() {
        setText("");
        validate();
        hideValidationErrors();
    }

    private void hideValidationErrors() {
        this.setBorder(Border.EMPTY);
    }

    private void checkRestriction(String oldString, String newString) {
        for (AbstractRestriction restriction : getRestrictions()) {
            this.ignoreRestrictions = true;
            restriction.checkRestriction(oldString, newString, textProperty());
            this.ignoreRestrictions = false;
        }
    }

    @Override
    public void validate() {
        validationErrorsProperty().clear();

        for (AbstractValidator validator : getValidators()) {
            validator.reset();
            if (!validator.isValid(getText())) {
                validationErrorsProperty().addAll(validator.getValidationErrors());
            }
        }
    }

    /*
     * Validators properties
     */
    public final void setValidators(ObservableList<AbstractValidator> value) {
        validatorsProperty().set(value);
    }

    public final ObservableList<AbstractValidator> getValidators() {
        return validators.get();
    }

    public final ObjectProperty<ObservableList<AbstractValidator>> validatorsProperty() {
        return validators;
    }

    /*
     * Restriction properties 
     */
    public final void setRestrictions(ObservableList<AbstractRestriction> value) {
        restrictionsProperty().set(value);
    }

    public final ObservableList<AbstractRestriction> getRestrictions() {
        return restrictions.get();
    }

    public final ObjectProperty<ObservableList<AbstractRestriction>> restrictionsProperty() {
        return restrictions;
    }

    /*
     * validation errors
     */
    public final void setValidationErrors(ObservableList<String> value) {
        validationErrorsProperty().set(value);
    }

    public final ObservableList<String> getValidationErrors() {
        return this.validationErrors.getValue();
    }

    public final ListProperty<String> validationErrorsProperty() {
        return this.validationErrors;
    }

    /*
     * valid
     */
    public final void setValid(boolean value) {
        validProperty().set(value);
    }

    public final boolean getValid() {
        return this.validProperty.getValue();
    }

    @Override
    public final BooleanProperty validProperty() {
        return this.validProperty;
    }
}
