package de.haack.fxviewlibrary.view.controls;

import de.haack.fxviewlibrary.view.validation.AbstractRestriction;
import de.haack.fxviewlibrary.view.validation.LengthRestriction;
import de.haack.fxviewlibrary.view.validation.NumRestrictor;
import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;

/**
 *
 * @author Kevin Haack
 */
public class RestrictedDatePicker extends DatePicker implements Validatable {

    private final StringProperty requiredTextProperty = new SimpleStringProperty();
    private final BooleanProperty requiredProperty = new SimpleBooleanProperty(this, "required");

    /**
     * textfield valid.
     */
    private final BooleanProperty validProperty = new SimpleBooleanProperty(this, "valid");
    /**
     * The current validation errors.
     */
    private final ListProperty<String> validationErrors = new SimpleListProperty(this, "validationErrors");
    /**
     * restrictors.
     */
    private final ObjectProperty<ObservableList<AbstractRestriction>> restrictions = new SimpleObjectProperty<>(this, "restriction");
    /**
     * ignor the restrictions, to prevent infinity loops.
     */
    private boolean ignoreRestrictions;
    /**
     * Tooltip for validation errors.
     */
    private final Tooltip tooltip = new Tooltip();

    public RestrictedDatePicker() {
        // init collections.
        restrictions.set(FXCollections.observableArrayList());
        setValidationErrors(FXCollections.observableArrayList());

        /*
         * valid on empty validationErrors or temporarily on focus and showing
         */
        validProperty().bind(validationErrorsProperty().emptyProperty()
                .or(this.focusedProperty())
                .or(this.showingProperty())
                .or(this.disabledProperty()));

        /*
         * validation errors
         */
        validationErrorsProperty().addListener((ListChangeListener.Change<? extends String> listener) -> {
            if (!listener.getList().isEmpty()) {
                /*
                 * @TODO Über CSS regeln
                 */
                final StringBuilder errorBuilder = new StringBuilder();
                listener.getList().forEach(error -> errorBuilder.append(error).append("\n"));

                tooltip.setText(errorBuilder.toString());

                showValidationErrors();
                getEditor().setTooltip(this.tooltip);
            } else {
                setTooltip(null);
                hideValidationErrors();
            }
        });

        /*
         * validators
         */
        getEditor().focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            // only validate when focuslost
            if (oldValue && !newValue) {
                validate();
            } else {
                hideValidationErrors();
            }
        });
        valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            validate();
        });

        /*
         * restritions 
         */
        getEditor().textProperty().addListener((ObservableValue<? extends String> observableValue, String oldString, String newString) -> {
            if (ignoreRestrictions || newString == null) {
                return;
            }

            checkRestriction(oldString, newString);
        });

        /*
         * add default restrictors
         */
        this.restrictions.get().add(new NumRestrictor("."));
        this.restrictions.get().add(new LengthRestriction(10));
    }

    /**
     * Reset the datepicker. Clear the editor and validate it.
     */
    public void reset() {
        setValue(null);
        validate();
        hideValidationErrors();
    }

    private void showValidationErrors() {
        Border border = new Border(new BorderStroke(javafx.scene.paint.Paint.valueOf("Red"),
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                BorderWidths.DEFAULT));
        this.setBorder(border);
    }

    private void hideValidationErrors() {
        this.setBorder(Border.EMPTY);
    }

    private void checkRestriction(String oldString, String newString) {
        for (AbstractRestriction restriction : restrictions.get()) {
            this.ignoreRestrictions = true;
            restriction.checkRestriction(oldString, newString, getEditor().textProperty());
            this.ignoreRestrictions = false;
        }
    }

    @Override
    public void validate() {
        validationErrors.clear();

        if (requiredProperty().get()) {
            if (null == getValue()) {
                getValidationErrors().add(requiredTextProperty.get());
            }
        }
    }

    /*
     * validation errors
     */
    public final void setValidationErrors(ObservableList<String> value) {
        validationErrorsProperty().set(value);
    }

    public final ObservableList<String> getValidationErrors() {
        return this.validationErrors.getValue();
    }

    public final ListProperty<String> validationErrorsProperty() {
        return this.validationErrors;
    }

    /*
     * valid
     */
    public final void setValid(boolean value) {
        validProperty().set(value);
    }

    public final boolean getValid() {
        return this.validProperty.getValue();
    }

    @Override
    public final BooleanProperty validProperty() {
        return this.validProperty;
    }

    /*
     * requiredText
     */
    public final void setRequiredText(String value) {
        requiredTextProperty().set(value);
    }

    public final String getRequiredText() {
        return this.requiredTextProperty.getValue();
    }

    public final StringProperty requiredTextProperty() {
        return this.requiredTextProperty;
    }

    /*
     * required
     */
    public final void setRequired(boolean value) {
        requiredProperty().set(value);
    }

    public final boolean getRequired() {
        return this.requiredProperty.getValue();
    }

    public final BooleanProperty requiredProperty() {
        return this.requiredProperty;
    }
}
