package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;

/**
 * A validator that requires this to be equal to antother <code>TextInputControl</code>.
 * 
 * 
 * @author Robin Marchel, Kevin Haack
 */
public class EqualsValidator extends AbstractValidator {

    public final StringProperty notEqualsTextProperty = new SimpleStringProperty();
    
    /**
     * The other field that has to be equal with this.
     */
    private final ObjectProperty<TextInputControl> field = new SimpleObjectProperty<>();;
    
    /**
     * Check wether the text is equal to the text from the other field.
     * 
     * @param text The text that has to be checked wether its valid or not.
     * @return true, if both field have the identical content.
     */
    @Override
    public boolean isValid(String text) {
        boolean valid = text.equals(field.get().getText());
        
        if (valid) {
            return true;
        }
        
        addValidationError(notEqualsTextProperty.get());
        return false;
    }
    
    /**
     * Setter for the field.
     * 
     * @param input The input field, that has to be equal to this.
     */
    public void setField(TextInputControl input) {
        this.field.set(input);
    }
    
    /**
     * Getter for the field.
     * 
     * @return Returns the field, that has to be equal to this.
     */
    public TextInputControl getField() {
        return field.get();
    }
    
    /**
     * Accessor for the field property.
     * 
     * @return The <code>ObjectProperty</code> that contains the field.
     */
    public ObjectProperty<TextInputControl> fieldProperty() {
        return field;
    }
    
    /*
     * notEqualsText
     */
    public final void setNotEqualsText(String value) {
        notEqualsTextProperty().set(value);
    }

    public final String getNotEqualsText() {
        return this.notEqualsTextProperty.getValue();
    }

    public final StringProperty notEqualsTextProperty() {
        return this.notEqualsTextProperty;
    }

}
