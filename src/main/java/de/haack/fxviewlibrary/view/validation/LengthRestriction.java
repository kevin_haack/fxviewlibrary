package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin Haack
 */
public class LengthRestriction extends AbstractRestriction {
    private final IntegerProperty maxLengthProperty = new SimpleIntegerProperty(0);
    
    public LengthRestriction(int maxLength) {
        setMaxLength(maxLength);
    }
    
    public LengthRestriction() {
        this(0);
    }
    
    public final void setMaxLength(int value) {
        maxLengthProperty().set(value);
    }
    public final int getMaxLength() {
        return maxLengthProperty.get();
    }
    public IntegerProperty maxLengthProperty() {
        return maxLengthProperty;
    }

    @Override
    public void checkRestriction(String oldString, String newString, StringProperty stringProperty) {
        if (newString.length() > getMaxLength()) {    
            stringProperty.set(newString.substring(0, getMaxLength()));
        }
    }
}
