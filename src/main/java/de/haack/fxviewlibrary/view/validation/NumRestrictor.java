package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This restrictor allows all chars that alphaNumeric.
 * You can specify a string which contains additional accpeted chars.
 * 
 * The chars are checked by <code>Character.isDigit()</code> which should be [0-9].
 * 
 * @author Kevin Haack
 */
public class NumRestrictor extends AbstractRestriction {

    /**
     * The additional chars that are also accepted.
     */
    private StringProperty additional;
    
    /**
     * Constructs a new restrictor which acceptes only alphaNumeric chars.
     */
    public NumRestrictor() {
        this("");
    }
    
    /**
     * Constructs a new restrictor which acceptes only alphaNumeric chars or one 
     * of the specified by the additional string.
     * 
     * @param additional All chars in this string will also be accepted by the restrictor.
     */
    public NumRestrictor(String additional) {
        this.additional = new SimpleStringProperty(additional);
    }

    /**
     * Checks the restriction of the string.
     * 
     * @param oldString The string that was in the field before.
     * @param newString The new string that should be checked.
     * @param stringProperty The property of the textfield, the restrictor sets the accepted chars
     * in this property so only the wrong will be filtered out.
     */
    @Override
    public void checkRestriction(String oldString, String newString, StringProperty stringProperty) {
        StringBuilder accepted = new StringBuilder();
        
        for (char c : newString.toCharArray()) {
            if (Character.isDigit(c)) {
                accepted.append(c);
            } else {
                for (char acceptedChar : additional.get().toCharArray()) {
                    if (c == acceptedChar) {
                        accepted.append(c);
                    }
                }
            }
        }
        
        stringProperty.set(accepted.toString());
    }

    /**
     * Returns the value of the additional string.
     * 
     * @return The additional chars as one string.
     */
    public String getAdditional() {
        return additional.get();
    }

    /**
     * Sets the value of the additional string.
     * 
     * @param add The additional chars as one string.
     */
    public void setAdditional(String add) {
        additional.set(add);
    }

    /**
     * Accesses the additionalProperty.
     * 
     * @return The additionalProperty.
     */
    public StringProperty additionalProperty() {
        return additional;
    }

}
