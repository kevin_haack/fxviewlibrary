package de.haack.fxviewlibrary.view.validation;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kevin Haack
 */
public abstract class AbstractValidator {
    /**
     * Contains all validations error strings for the UI.
     */
    private final List<String> validationErrors = new ArrayList<>();

    public AbstractValidator() {
    }

    /**
     * Check that the passed string is valid.
     * @param text
     * @return 
     */
    public abstract boolean isValid(String text);

    /**
     * Return all validation errors.
     * @return 
     */
    public List<String> getValidationErrors() {
        return validationErrors;
    }

    /**
     * Add the passed validation error string for the UI.
     * @param error 
     */
    protected void addValidationError(String error) {
        if(null == error) {
            throw new IllegalArgumentException("The passed error string was null.");
        }
        
        this.validationErrors.add(error);
    }
    
    public void  reset() {
        this.validationErrors.clear();
    }
}
