package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin
 */
public abstract class AbstractRestriction {
    public abstract void checkRestriction(String oldString, String newString, StringProperty stringProperty);
}
