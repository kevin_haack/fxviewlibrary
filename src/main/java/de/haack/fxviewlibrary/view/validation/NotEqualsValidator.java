package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Kevin Haack
 */
public class NotEqualsValidator extends AbstractValidator {

    private final ObjectProperty<ObservableList<String>> valuesProperty = new SimpleObjectProperty<>(this, "values");

    public final StringProperty alreadyExistTextProperty = new SimpleStringProperty();
    public final BooleanProperty caseSensitiveProperty = new SimpleBooleanProperty();

    public NotEqualsValidator() {
    }

    public NotEqualsValidator(String alreadyExistText, boolean caseSensitive) {
        setAlreadyExistText(alreadyExistText);
        setCaseSensitive(caseSensitive);
    }

    @Override
    public boolean isValid(String text) {
        boolean valid = true;

        if (caseSensitiveProperty.get()) {
            for (String s : valuesProperty.get()) {
                if (s.equals(text)) {
                    valid = false;
                }
            }
        } else {
            text = text.toLowerCase();
            for (String s : valuesProperty.get()) {
                if (s.toLowerCase().equals(text)) {
                    valid = false;
                }
            }
        }

        if (!valid) {
            addValidationError(this.alreadyExistTextProperty.get());
        }
        
        return valid;
    }

    /*
     * properties
     */
    public final void setValues(ObservableList<String> value) {
        valuesProperty().set(value);
    }

    public final ObservableList<String> getValues() {
        return valuesProperty.get();
    }

    public final ObjectProperty<ObservableList<String>> valuesProperty() {
        return valuesProperty;
    }

    public final void setAlreadyExistText(String value) {
        alreadyExistTextProperty().set(value);
    }

    public final String getAlreadyExistText() {
        return this.alreadyExistTextProperty.getValue();
    }

    public final StringProperty alreadyExistTextProperty() {
        return this.alreadyExistTextProperty;
    }

    public final void setCaseSensitive(boolean value) {
        caseSensitiveProperty().set(value);
    }

    public final boolean getCaseSensitive() {
        return this.caseSensitiveProperty.getValue();
    }

    public final BooleanProperty caseSensitiveProperty() {
        return this.caseSensitiveProperty;
    }
}
