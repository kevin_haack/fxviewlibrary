package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin Haack
 */
public class RegexRestriction extends AbstractRestriction {

    private final StringProperty regexProperty = new SimpleStringProperty("");

    public final void setRegex(String value) {
        regexProperty().set(value);
    }

    public final String getRegex() {
        return regexProperty.get();
    }

    public StringProperty regexProperty() {
        return regexProperty;
    }

    @Override
    public void checkRestriction(String oldString, String newString, StringProperty stringProperty) {
        if (!getRegex().equals("")
                && !newString.matches(getRegex() + "*")) {
            stringProperty.set(oldString);
        }
    }
}
