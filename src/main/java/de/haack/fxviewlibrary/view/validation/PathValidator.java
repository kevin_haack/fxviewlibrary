package de.haack.fxviewlibrary.view.validation;

import java.io.File;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin Haack
 */
public class PathValidator extends AbstractValidator {

    public final StringProperty notExistTextProperty = new SimpleStringProperty();
    public final StringProperty isFolderTextProperty = new SimpleStringProperty();
    public final StringProperty isFileTextProperty = new SimpleStringProperty();

    private final BooleanProperty acceptFilesProperty = new SimpleBooleanProperty(true);
    private final BooleanProperty acceptFoldersProperty = new SimpleBooleanProperty(true);

    @Override
    public boolean isValid(String text) {
        boolean valid = true;

        File file = new File(text);

        if (file.exists()) {
            if (!getAcceptFiles()
                    && file.isFile()) {
                valid = false;
                addValidationError(isFileTextProperty.get());
            }

            if (!getAcceptFolders()
                    && file.isDirectory()) {
                valid = false;
                addValidationError(isFolderTextProperty.get());
            }
        } else {
            valid = false;
            addValidationError(notExistTextProperty.get());
        }

        return valid;
    }
    
    /*
     * isFolderText
     */
    public final void setIsFolderText(String value) {
        isFolderTextProperty().set(value);
    }

    public final String getIsFolderText() {
        return this.isFolderTextProperty.getValue();
    }

    public final StringProperty isFolderTextProperty() {
        return this.isFolderTextProperty;
    }
    
    /*
     * isFileText
     */
    public final void setIsFileText(String value) {
        isFileTextProperty().set(value);
    }

    public final String getIsFileText() {
        return this.isFileTextProperty.getValue();
    }

    public final StringProperty isFileTextProperty() {
        return this.isFileTextProperty;
    }
    
    /*
     * notExistText
     */
    public final void setNotExistText(String value) {
        notExistTextProperty().set(value);
    }

    public final String getNotExistText() {
        return this.notExistTextProperty.getValue();
    }

    public final StringProperty notExistTextProperty() {
        return this.notExistTextProperty;
    }

    /*
     * accept files.
     */
    public final void setAcceptFiles(boolean value) {
        acceptFilesProperty().set(value);
    }

    public final boolean getAcceptFiles() {
        return acceptFilesProperty.get();
    }

    public BooleanProperty acceptFilesProperty() {
        return acceptFilesProperty;
    }

    /*
     * accept folder.
     */
    public final void setAcceptFolders(boolean value) {
        acceptFoldersProperty().set(value);
    }

    public final boolean getAcceptFolders() {
        return acceptFoldersProperty.get();
    }

    public BooleanProperty acceptFoldersProperty() {
        return acceptFoldersProperty;
    }
}
