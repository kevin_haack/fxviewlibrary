package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Kevin Haack
 */
public class SecurityPoliciesValidator extends AbstractValidator {

    public final StringProperty countOfCharClassesTextProperty = new SimpleStringProperty();
    public final StringProperty minimumLengthTextProperty = new SimpleStringProperty();
    public final StringProperty maximumLengthTextProperty = new SimpleStringProperty();

    private final IntegerProperty minimimCharacterCountProperty = new SimpleIntegerProperty(8);
    private final IntegerProperty maximumCharacterCountProperty = new SimpleIntegerProperty(-1);

    private final IntegerProperty charClasses = new SimpleIntegerProperty(3);

    public SecurityPoliciesValidator() {
        /*
         * Restrict the value of the char classes from 1 to 4
         */
        charClasses.addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() > 4 || newValue.intValue() < 1) {
                    charClasses.set(1);
                    throw new IllegalArgumentException("The char classes has to be between 1 and 4");
                }
            }
        });
    }

    @Override
    public boolean isValid(String text) {
        boolean isValid = true;

        /*
         * Check the chars for the char classes.
         */
        boolean lowerCase = false;
        boolean upperCase = false;
        boolean digit = false;
        boolean specialChars = false;

        for (char c : text.toCharArray()) {
            if (Character.isLowerCase(c)) {
                lowerCase = true;
            } else if (Character.isUpperCase(c)) {
                upperCase = true;
            } else if (Character.isDigit(c)) {
                digit = true;
            } else {
                specialChars = true;
            }
        }

        //Check the different class count
        if ((lowerCase ? 1 : 0) + (upperCase ? 1 : 0) + (digit ? 1 : 0) + (specialChars ? 1 : 0) < charClasses.get()) {
            addValidationError(countOfCharClassesTextProperty.get());
            isValid = false;
        }

        // Check the min length
        if (text.length() < minimimCharacterCountProperty.get()) {
            addValidationError(minimumLengthTextProperty.get());
            isValid = false;
        }

        //Check the max length
        if (maximumCharacterCountProperty.get() != -1 && text.length() > maximumCharacterCountProperty.get()) {
            addValidationError(maximumLengthTextProperty.get());
            isValid = false;
        }

        return isValid;
    }

    /*
     * maximumLengthText
     */
    public final void setMaximumLengthText(String value) {
        maximumLengthTextProperty().set(value);
    }

    public final String getMaximumLengthText() {
        return this.maximumLengthTextProperty.getValue();
    }

    public final StringProperty maximumLengthTextProperty() {
        return this.maximumLengthTextProperty;
    }

    /*
     * minimumLengthText
     */
    public final void setMinimumLengthText(String value) {
        minimumLengthTextProperty().set(value);
    }

    public final String getMinimumLengthText() {
        return this.minimumLengthTextProperty.getValue();
    }

    public final StringProperty minimumLengthTextProperty() {
        return this.minimumLengthTextProperty;
    }

    /*
     * countOfCharClassesText
     */
    public final void setCountOfCharClassesText(String value) {
        countOfCharClassesTextProperty().set(value);
    }

    public final String getCountOfCharClassesText() {
        return this.countOfCharClassesTextProperty.getValue();
    }

    public final StringProperty countOfCharClassesTextProperty() {
        return this.countOfCharClassesTextProperty;
    }

    /*
     * maximumCharacterCount
     */
    public final void setMaximumCharacterCount(int value) {
        maximumCharacterCountProperty().set(value);
    }

    public final int getMaximumCharacterCount() {
        return maximumCharacterCountProperty.get();
    }

    public IntegerProperty maximumCharacterCountProperty() {
        return maximumCharacterCountProperty;
    }

    /*
     * minimumCharacterCount
     */
    public final void setMinimimCharacterCount(int value) {
        minimimCharacterCountProperty().set(value);
    }

    public final int getMinimimCharacterCount() {
        return minimimCharacterCountProperty.get();
    }

    public IntegerProperty minimimCharacterCountProperty() {
        return minimimCharacterCountProperty;
    }

    /*
     * charClasses
     */
    public final void setCharClasses(int i) {
        charClasses.set(i);
    }

    public final int getCharClasses() {
        return charClasses.get();
    }

    public IntegerProperty charClassesProperty() {
        return charClasses;
    }
}
