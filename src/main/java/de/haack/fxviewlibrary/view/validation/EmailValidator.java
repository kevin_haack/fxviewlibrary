package de.haack.fxviewlibrary.view.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Robin Marchel, Kevin Haack
 */
public class EmailValidator extends AbstractValidator {
    public final StringProperty notValidTextProperty = new SimpleStringProperty();

    private static Pattern compiledPattern;

    @Override
    public boolean isValid(String text) {

        if (compiledPattern == null) {
            String emailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            compiledPattern = Pattern.compile(emailRegex);
        }
        
        Matcher mather = compiledPattern.matcher(text);
        if (mather.matches()) {
            return true;
        } else {
            addValidationError(notValidTextProperty.get());
            return false;
        }
    }

    /*
     * isFolderText
     */
    public final void setNotValidText(String value) {
        notValidTextProperty().set(value);
    }

    public final String getNotValidText() {
        return this.notValidTextProperty.getValue();
    }

    public final StringProperty notValidTextProperty() {
        return this.notValidTextProperty;
    }
}
