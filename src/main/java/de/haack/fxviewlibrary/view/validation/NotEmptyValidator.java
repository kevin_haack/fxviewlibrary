package de.haack.fxviewlibrary.view.validation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin Haack
 */
public class NotEmptyValidator extends AbstractValidator {
    /**
     * The validation text for the UI.
     */
    public final StringProperty emptyTextProperty = new SimpleStringProperty();

    public NotEmptyValidator() {
    }
    
    public NotEmptyValidator(String emptyText) {
        emptyTextProperty.set(emptyText);
    }
    
    @Override
    public boolean isValid(String text) {
        if (text != null && !text.isEmpty()) {
            return true;
        }
        
        addValidationError(this.emptyTextProperty.get());
        return false;
    }
    
    /*
     * emptyText
     */
    public final void setEmptyText(String value) {
        emptyTextProperty().set(value);
    }

    public final String getEmptyText() {
        return this.emptyTextProperty.getValue();
    }

    public final StringProperty emptyTextProperty() {
        return this.emptyTextProperty;
    }
}
