package de.haack.fxviewlibrary.view;

import javafx.beans.binding.BooleanBinding;

/**
 *
 * @author Kevin haack
 */
public abstract class AbstractAction {
    private final String action;
    private final BooleanBinding disableBinding;
    
    public abstract void onAction();

    public String getAction() {
        return action;
    }

    public BooleanBinding getDisableBinding() {
        return disableBinding;
    }
    
    public AbstractAction() {
        this(null);
    }
    
    public AbstractAction(String action) {
        this(action, null);
        
    }
    
    public AbstractAction(String action, BooleanBinding disableBinding) {
        this.action = action;
        this.disableBinding = disableBinding;
    }
}
