package de.haack.fxviewlibrary.view.dialog;

import de.haack.fxviewlibrary.AuthenticationFailedException;
import de.haack.fxviewlibrary.DatabaseNotReachableException;
import de.haack.fxviewlibrary.ResourceManager;
import de.haack.fxviewlibrary.view.AbstractAction;
import javafx.scene.Node;

/**
 *
 * @author Kevin Haack
 */
public class DialogFactory {
    
    private static String cssPath;

    private DialogFactory() {

    }

    public static String getCssPath() {
        return cssPath;
    }

    public static void setCssPath(String cssPath) {
        DialogFactory.cssPath = cssPath;
    }
    
    public static CustomDialog createCustomDialog(String title, String header, String detail, AbstractAction onAccept) {
        return new CustomDialog(title, header, detail, onAccept, cssPath);
    }

    public static CustomDialog createCustomDialog(String title, String header, String detail, AbstractAction onAccept, AbstractAction onCancel, Node... nodes) {
        return new CustomDialog(title, header, detail, onAccept, onCancel, cssPath, nodes);
    }
    
    public static SimpleDialog createSimpleDialog(String title, String header, String detail, AbstractAction onAccept) {
        return new SimpleDialog(title, header, detail, onAccept, cssPath);
    }
    
    public static SimpleDialog createSimpleDialog(String title, String header, String detail, AbstractAction onAccept, AbstractAction onCancel) {
        return new SimpleDialog(title, header, detail, onAccept, onCancel, cssPath);
    }

    public static SimpleDialog createSimpleDialog(String title, String header, String detail, AbstractAction onAccept, AbstractAction onNeutral, AbstractAction onCancel) {
        return new SimpleDialog(title, header, detail, onAccept, onNeutral, onCancel, cssPath);
    }

    private static SimpleDialog createAuthenticationFailedDialog() {
        String title = ResourceManager.getString("DialogFactory.authenticationFailed.title");
        String details = ResourceManager.getString("DialogFactory.authenticationFailed.details");
        String ok = ResourceManager.getString("DialogFactory.authenticationFailed.action.ok");

        AbstractAction action = new AbstractAction(ok) {
            @Override
            public void onAction() {

            }
        };

        return new SimpleDialog(title, title, details, action, cssPath);
    }

    private static SimpleDialog createDatabaseNotReachableDialog() {
        String title = ResourceManager.getString("DialogFactory.databaseNotReachable.title");
        String details = ResourceManager.getString("DialogFactory.databaseNotReachable.details");
        String ok = ResourceManager.getString("DialogFactory.databaseNotReachable.action.ok");

        AbstractAction action = new AbstractAction(ok) {
            @Override
            public void onAction() {

            }
        };

        return new SimpleDialog(title, title, details, action, cssPath);
    }

    public static SimpleDialog createUnexpectedExceptionDialog() {
        String title = ResourceManager.getString("DialogFactory.unexpected.title");
        String details = ResourceManager.getString("DialogFactory.unexpected.details");
        String ok = ResourceManager.getString("DialogFactory.unexpected.action.ok");

        AbstractAction action = new AbstractAction(ok) {
            @Override
            public void onAction() {

            }
        };

        return new SimpleDialog(title, title, details, action, cssPath);
    }

    public static SimpleDialog createDefaultExceptionDialog(Throwable exception) {
        SimpleDialog dialog = null;

        if (exception != null) {
            if (exception instanceof AuthenticationFailedException) {
                dialog = createAuthenticationFailedDialog();
            } else if (exception instanceof DatabaseNotReachableException) {
                dialog = createDatabaseNotReachableDialog();
            } else {
                exception.printStackTrace();
                dialog = createUnexpectedExceptionDialog();
            }
        }

        return dialog;

    }
}
