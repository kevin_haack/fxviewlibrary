package de.haack.fxviewlibrary.view.dialog;

import de.haack.fxviewlibrary.view.AbstractAction;
import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.annotation.ElementController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Kevin Haack
 */
@ElementController(
        FXML = "/fxml/dialog/SimpleDialog.fxml"
)
public class SimpleDialog extends AnchorPane {

    private Stage stage;

    @FXML
    private Label detail;
    @FXML
    private Label header;
    @FXML
    private Button accept;
    @FXML
    private Button cancel;
    @FXML
    private Button neutral;

    protected SimpleDialog(String title, String header, String detail, AbstractAction onAccept, String css) {
        this(title, header, detail, onAccept, null, css);
    }

    protected SimpleDialog(String title, String header, String detail, AbstractAction onAccept, AbstractAction onCancel, String css) {
        this(title, header, detail, onAccept, null, onCancel, css);
    }

    protected SimpleDialog(String title, String header, String detail, AbstractAction onAccept, AbstractAction onNeutral, AbstractAction onCancel, String css) {
        // FXML
        ViewManager.loadElementView(SimpleDialog.class, this, css);

        this.stage = new Stage();
        this.stage.setScene(new Scene(this));
        this.stage.setTitle(title);
        this.stage.setMaximized(false);
        this.stage.initModality(Modality.APPLICATION_MODAL);
        this.stage.setResizable(false);

        // Header
        if (null != header) {
            this.header.setText(header);
        }

        // on close
        getScene().getWindow().setOnCloseRequest((WindowEvent event) -> {
            if (null != onCancel) {
                onCancel.onAction();
            } else if (null != onAccept) {
                onAccept.onAction();
            }
        });

        // Detail
        if (null != detail) {
            this.detail.setText(detail);
        } else {
            ((VBox) this.detail.getParent()).getChildren().remove(this.detail);
        }

        // on accept
        if (null != onAccept) {
            this.accept.setOnKeyPressed((KeyEvent event) -> {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    stage.hide();
                    onAccept.onAction();
                }
            });

            if (null != onAccept.getAction()) {
                this.accept.setText(onAccept.getAction());
            }

            if (null != onAccept.getDisableBinding()) {
                this.accept.disableProperty().bind(onAccept.getDisableBinding());
            }

            this.accept.onActionProperty().set((EventHandler<ActionEvent>) (ActionEvent event) -> {
                stage.hide();
                onAccept.onAction();
            });
        }

        // on neutral
        if (null != onNeutral) {
            this.neutral.setOnKeyPressed((KeyEvent event) -> {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    stage.hide();
                    onNeutral.onAction();
                }
            });

            if (null != onNeutral.getAction()) {
                this.neutral.setText(onNeutral.getAction());
            }

            if (null != onNeutral.getDisableBinding()) {
                this.neutral.disableProperty().bind(onNeutral.getDisableBinding());
            }

            this.neutral.onActionProperty().set((EventHandler<ActionEvent>) (ActionEvent event) -> {
                stage.hide();
                onNeutral.onAction();
            });
        } else {
            ((HBox) this.neutral.getParent()).getChildren().remove(this.neutral);
        }

        // on cancel
        if (null != onCancel) {
            this.cancel.setOnKeyPressed((KeyEvent event) -> {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    stage.hide();
                    onCancel.onAction();
                }
            });

            if (null != onCancel.getAction()) {
                this.cancel.setText(onCancel.getAction());
            }

            if (null != onCancel.getDisableBinding()) {
                this.cancel.disableProperty().bind(onCancel.getDisableBinding());
            }

            this.cancel.onActionProperty().set((EventHandler<ActionEvent>) (ActionEvent event) -> {
                stage.hide();
                onCancel.onAction();
            });
        } else {
            ((HBox) this.cancel.getParent()).getChildren().remove(this.cancel);
        }
    }

    public void show() {
        this.stage.show();
    }

    public void hide() {
        this.stage.hide();
    }
}
