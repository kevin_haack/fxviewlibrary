package de.haack.fxviewlibrary.view.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javafx.stage.Modality;

/**
 *
 * @author Kevin Haack
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SubViewController {
    
    String FXML();
    
    String Title() default "";
    
    boolean FullScreenByDefault() default false;
    
    boolean ResizeableByDefault() default true;
    
    boolean MaximizedByDefault() default false;
    
    Modality Modality() default Modality.NONE;
    
    String ProgressIndicatorImageName() default "";
}

