package de.haack.fxviewlibrary.view;

import de.haack.fxviewlibrary.ResourceManager;
import static de.haack.fxviewlibrary.view.ViewManager.getLanguageFile;
import de.haack.fxviewlibrary.view.annotation.ElementController;
import de.haack.fxviewlibrary.view.annotation.MainViewController;
import de.haack.fxviewlibrary.view.annotation.SubViewController;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Robin Marchel, Kevin Haack
 */
public class ViewManager {

    /**
     * The language file.
     */
    public static String languageFile = "lang.lang";
    /**
     * Current locale.
     */
    public static Locale locale = Locale.GERMAN;
    /**
     * The primary stage. To display all mainViews.
     */
    private static Stage primaryStage;
    /**
     * The main view controller cache.
     */
    private static final Map<Class, AbstractController> mainViewControllers = new HashMap<>();
    /**
     * The main view scene cache.
     */
    private static final Map<Class, Scene> mainViewScenes = new HashMap<>();
    /**
     * The sub view controller cache.
     */
    private static final Map<Class, AbstractController> subViewControllers = new HashMap<>();
    /**
     * The sub view scene cache.
     */
    private static final Map<Class, Stage> subViewStages = new HashMap<>();
    /**
     * Action to be invoked on every user interaction.
     */
    private static AbstractAction onUserInteraction = null;

    private ViewManager() {
    }

    public static String getLanguageFile() {
        return languageFile;
    }

    public static void setLanguageFile(String languageFile) {
        ViewManager.languageFile = languageFile;
    }

    public static AbstractAction getOnUserInteraction() {
        return onUserInteraction;
    }

    public static void setOnUserInteraction(AbstractAction onUserInteraction) {
        ViewManager.onUserInteraction = onUserInteraction;
    }

    public static void loadElementView(Class<? extends Node> controllerClass, Parent controller) {
        loadElementView(controllerClass, controller, null);
    }

    public static void loadElementView(Class<? extends Node> controllerClass, Parent controller, String css) {

        validateState();

        if (!controllerClass.isAnnotationPresent(ElementController.class)) {
            throw new IllegalArgumentException("Class do not use 'ElementController' annotation.");
        }

        ElementController annotation = (ElementController) controllerClass.getAnnotation(ElementController.class);
        /*
         * load resources
         */
        FXMLLoader loader = new FXMLLoader(ViewManager.class.getResource(annotation.FXML()));
        loader.setResources(ResourceBundle.getBundle(getLanguageFile(), getLocale()));

        loader.setRoot(controller);
        loader.setController(controller);
        
        /*
         * optional css
         */
        if (null != css
                && !css.isEmpty()) {
            controller.getStylesheets().add(css);
        }

        try {
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load ElementView (" + ex.getMessage() + ").", ex);
        }
    }

    private static Node getProgressIndicator(String progressIndicatorImageName) {
        Node progressIndicator;

        if (null == progressIndicatorImageName
                || progressIndicatorImageName.isEmpty()) {
            // Default
            ProgressIndicator def = new ProgressIndicator();
            def.setMaxSize(200, 200);
            def.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            def.setVisible(false);

            progressIndicator = def;
        } else {
            ImageView image = new ImageView(ResourceManager.getImage(progressIndicatorImageName));
            image.setVisible(false);

            progressIndicator = image;
        }

        return progressIndicator;
    }

    private static void createMainView(Class<? extends AbstractController> controllerClass) {

        try {
            if (!controllerClass.isAnnotationPresent(MainViewController.class)) {
                throw new IllegalArgumentException("Class do not use 'MainViewController' annotation.");
            }

            MainViewController annotation = (MainViewController) controllerClass.getAnnotation(MainViewController.class);

            /*
             * load resources
             */
            FXMLLoader loader = new FXMLLoader(ViewManager.class.getResource(annotation.FXML()));
            loader.setResources(ResourceBundle.getBundle(getLanguageFile(), getLocale()));

            /*
             * build content
             */
            StackPane stackPane = new StackPane();
            Node content = new Scene((Parent) loader.load()).getRoot();
            Node progressIndicator = getProgressIndicator(annotation.ProgressIndicatorImageName());

            stackPane.getChildren().add(progressIndicator);
            stackPane.getChildren().add(content);

            // anchorPane as root, so you can absolutely position elements
            AnchorPane root = new AnchorPane(stackPane);

            AnchorPane.setBottomAnchor(stackPane, 0D);
            AnchorPane.setTopAnchor(stackPane, 0D);
            AnchorPane.setLeftAnchor(stackPane, 0D);
            AnchorPane.setRightAnchor(stackPane, 0D);

            /*
             * setup scene
             */
            Scene scene = new Scene(root);

            scene.setOnMouseMoved((MouseEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnKeyTyped((KeyEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnMouseClicked((MouseEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnScroll((ScrollEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });

            /*
             * inject config and stage.
             */
            try {
                AbstractController ctr = loader.getController();

                ctr.setStage(ViewManager.primaryStage);
                ctr.setProgressIndicator(progressIndicator);
                ctr.setContent(content);

                mainViewControllers.put(controllerClass, ctr);
                mainViewScenes.put(controllerClass, scene);
            } catch (ClassCastException ex) {
                throw new UnsupportedControllerException("The controller musst extends the AbstractController.", ex);
            }

        } catch (IOException ex) {
            throw new RuntimeException("Failed to create MainView (" + ex.getMessage() + ").", ex);
        }

    }
    
    private static void validateState() {
        if (null == getLocale()) {
            throw new IllegalStateException("Location is not set. (null)");
        }

        if (null == getLanguageFile()
                || getLanguageFile().isEmpty()) {
            throw new IllegalStateException("Language file is not set. (null or empty string)");
        }
    }

    /**
     * Load the passed MainView in the primaryStage
     *
     * @param controllerClass
     * @param config
     */
    public static void loadMainView(Class<? extends AbstractController> controllerClass, ViewConfig config) throws UnsupportedControllerException {
        
        validateState();

        if (!controllerClass.isAnnotationPresent(MainViewController.class)) {
            throw new IllegalArgumentException("Class do not use 'MainViewController' annotation.");
        }

        MainViewController annotation = (MainViewController) controllerClass.getAnnotation(MainViewController.class);

        AbstractController controller;
        Scene scene;

        if (!(mainViewControllers.containsKey(controllerClass)
                || mainViewScenes.containsKey(controllerClass))) {
            createMainView(controllerClass);

            controller = mainViewControllers.get(controllerClass);
            scene = mainViewScenes.get(controllerClass);

            controller.setViewConfig(config);
            controller.onPostInitialization();
        } else {
            controller = mainViewControllers.get(controllerClass);
            scene = mainViewScenes.get(controllerClass);

            controller.setViewConfig(config);
            controller.onReset();
        }

        /*
         * setup primary stage
         */
        ViewManager.primaryStage.setScene(scene);
        ViewManager.primaryStage.setTitle(ResourceManager.getString(annotation.Title()));

        double oldHeight = ViewManager.primaryStage.getHeight();
        double oldWidth = ViewManager.primaryStage.getWidth();

        // the size must be set here, if you tries to set the size in the scene constructor
        // you get graphic bugs (e.g. a black view on focus lost or a black view on some mouse hovers).
        // Tested on Windows 7.
        ViewManager.primaryStage.setHeight(oldHeight);
        ViewManager.primaryStage.setWidth(oldWidth);

        /*
         * inform controller.
         */
        controller.onPostLoad();
    }

    public static void setPrimaryStage(Stage primaryStage) {
        ViewManager.primaryStage = primaryStage;
    }

    public static Locale getLocale() {
        return locale;
    }

    public static void setLocale(Locale locale) {
        ViewManager.locale = locale;
    }

    public static class UnsupportedControllerException extends RuntimeException {

        public UnsupportedControllerException(String message, Throwable cause) {
            super(message);
            this.initCause(cause);
        }
    }

    private static void createSubView(Class<? extends AbstractController> controllerClass) {

        try {
            if (!controllerClass.isAnnotationPresent(SubViewController.class)) {
                throw new IllegalArgumentException("Class do not use 'SubViewController' annotation.");
            }

            SubViewController annotation = (SubViewController) controllerClass.getAnnotation(SubViewController.class);
            
            /*
             * setup stage
             */
            Stage stage = new Stage();
            stage.setTitle(ResourceManager.getString(annotation.Title()));
            stage.setMaximized(annotation.MaximizedByDefault());
            stage.initModality(annotation.Modality());
            stage.setResizable(annotation.ResizeableByDefault());
            stage.setFullScreen(annotation.FullScreenByDefault());

            /*
             * load resources
             */
            FXMLLoader loader = new FXMLLoader(ViewManager.class.getResource(annotation.FXML()));
            loader.setResources(ResourceBundle.getBundle(getLanguageFile(), getLocale()));

            Node content = new Scene(loader.load()).getRoot();
            Node progressIndicator = getProgressIndicator(annotation.ProgressIndicatorImageName());

            StackPane stackPane = new StackPane();
            stackPane.getChildren().add(progressIndicator);
            stackPane.getChildren().add(content);

            /*
             * setup scene
             */
            Scene scene = new Scene(stackPane);

            scene.setOnMouseMoved((MouseEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnKeyTyped((KeyEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnMouseClicked((MouseEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });
            scene.setOnScroll((ScrollEvent event) -> {
                if (null != ViewManager.onUserInteraction) {
                    ViewManager.onUserInteraction.onAction();
                }
            });

            stage.setScene(scene);
            
            /*
             * inject config and stage.
             */
            try {
                AbstractController ctr = loader.getController();

                ctr.setStage(stage);
                ctr.setProgressIndicator(progressIndicator);
                ctr.setContent(content);

                subViewControllers.put(controllerClass, ctr);
                subViewStages.put(controllerClass, stage);
            } catch (ClassCastException ex) {
                throw new UnsupportedControllerException("The controller musst extends the AbstractController.", ex);
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to create SubView (" + ex.getMessage() + ").", ex);
        }
    }

    /**
     * Return the passed subview in a view stage.
     *
     * @param controllerClass
     * @param config
     * @return the new stage.
     */
    public static Stage getSubView(Class<? extends AbstractController> controllerClass, ViewConfig config) {
        if (!controllerClass.isAnnotationPresent(SubViewController.class)) {
            throw new IllegalArgumentException("Class do not use 'SubViewController' annotation.");
        }

        AbstractController controller;
        Stage stage;

        if (!(subViewControllers.containsKey(controllerClass)
                || subViewStages.containsKey(controllerClass))) {
            createSubView(controllerClass);

            controller = subViewControllers.get(controllerClass);
            stage = subViewStages.get(controllerClass);

            controller.setViewConfig(config);
            controller.onPostInitialization();
        } else {
            controller = subViewControllers.get(controllerClass);
            stage = subViewStages.get(controllerClass);

            controller.setViewConfig(config);
            controller.onReset();
        }

        /*
         * inform controller.
         */
        controller.onPostLoad();

        return stage;
    }
}
