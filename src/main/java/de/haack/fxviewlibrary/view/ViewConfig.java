package de.haack.fxviewlibrary.view;

import java.util.HashMap;

/**
 *
 * @author Robin Marchel, Kevin Haack
 */
public class ViewConfig {
    private HashMap<String, Object> configs;
    
    public ViewConfig() {
        this.configs = new HashMap<>();
    }
    
    public ViewConfig(String key, Object value) {
        this();
        this.set(key, value);
    }
    
    public final void set(String key, Object value) {
        this.configs.put(key, value);
    }
    
    public final Object read(String key) {
        if (this.configs.containsKey(key)) {
            return this.configs.get(key);
        }
        return null;
    }
    
    public final <T extends Object> T read(String key, Class<T> type) {
        if (this.configs.containsKey(key) && type.isInstance(this.configs.get(key))) {
            return (T) this.configs.get(key);
        }
        return null;
    }
    
    public final boolean contains(String key) {
        return this.configs.containsKey(key)
                && null != this.configs.get(key);
    }
}
