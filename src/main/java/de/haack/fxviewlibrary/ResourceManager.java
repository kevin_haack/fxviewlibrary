package de.haack.fxviewlibrary;

import de.haack.fxviewlibrary.view.ViewManager;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.scene.image.Image;

/**
 * This class handle all resources and manage already loaded resources, like
 * images.
 *
 * @author Kevin Haack
 */
public class ResourceManager {

    /**
     * The placeholder for strings.
     */
    private static final String PLACEHOLDER = "\\{\\?\\}";
    /**
     * The path to the graphics.
     */
    private static String graphicPath = "/graphics/";
    /**
     * The path to the html documents.
     */
    private static String documentsPath = "/htmlDocument/";
    /**
     * Already loaded images.
     */
    private final static HashMap<String, Image> images = new HashMap<>();

    private ResourceManager() {

    }

    /**
     * Return the passed image from the resources.
     *
     * @param name
     * @return
     */
    public static Image getImage(String name) {
        if (images.containsKey(name)) {
            return images.get(name);
        } else {
            Image image = new Image(ResourceManager.class.getResourceAsStream(getGraphicPath() + name));
            images.put(name, image);

            return image;
        }

    }

    public static String getDocumentsPath() {
        return documentsPath;
    }

    public static void setDocumentsPath(String HTMLDocumentPath) {
        ResourceManager.documentsPath = HTMLDocumentPath;
    }

    public static String getGraphicPath() {
        return graphicPath;
    }

    public static void setGraphicPath(String graphicPath) {
        ResourceManager.graphicPath = graphicPath;
    }

    /**
     * Return the passed document from the the resources.
     *
     * @param name
     * @param locale
     * @return
     */
    public static URL getDocumentPath(String name, Locale locale) {
        // TODO i18n
        return ResourceManager.class.getResource(getDocumentsPath() + name + "_de.html");
    }

    /**
     * Return the String to the passed key from the ViewManager.LANGUAGE_FILE
     * ResourceBundle.
     *
     * @param key key.
     * @return String.
     */
    public static String getString(String key) {
        String[] empty = {};
        return getString(key, empty);
    }

    /**
     * Return the String to the passed key from the ViewManager.LANGUAGE_FILE
     * ResourceBundle and replace the placeholder.
     *
     * @param key
     * @param params
     * @return
     */
    public static String getString(String key, String... params) {
        ResourceBundle bundle = ResourceBundle.getBundle(ViewManager.getLanguageFile(), ViewManager.getLocale());
        String string;

        if (bundle.containsKey(key)) {
            string = bundle.getString(key);
            for (String param : params) {
                string = string.replaceFirst(PLACEHOLDER, param);
            }

        } else {
            string = key;
        }

        return string;
    }

    /**
     * HTML Doucments
     */
    public enum HTMLDocuments {

        FAQ("faq", "HTMLDocument.faq.title"),
        ApplicationNotes("applicationNotes", "HTMLDocument.applicationNotes.title"),
        PrivacyNotes("privacyNotes", "HTMLDocument.privacyNotes.title");

        private final String document;
        private final String title;

        private HTMLDocuments(String document, String title) {
            this.document = document;
            this.title = title;
        }

        public String getDocument() {
            return document;
        }

        public String getTitle() {
            return title;
        }
    }
}
